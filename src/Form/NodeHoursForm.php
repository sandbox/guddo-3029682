<?php

namespace Drupal\opening_hours\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\opening_hours\Services\DateHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class NodeHoursForm extends FormBase
{

  const FORM_ID = 'opening_hours.node_hours_form';

  const CONFIG_KEY = 'opening_hours.node_hours';

  /**
   * @var \Drupal\opening_hours\Services\DateHelper
   */
  private $dateHelper;

  public function __construct(DateHelper $dh) {
    $this->dateHelper = $dh;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('opening_hours_date_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL, NodeInterface $node = NULL) {
    $year = (int) $request->query->get('year');
    $week = (int) $request->query->get('week');

    $now = new \DateTime();
    $now->setISODate($year, $week);

    $isoWeekNumber = $now->format('W');
    $isoYear = $now->format('o');

    $form_state->set('selected_week', $isoWeekNumber);
    $form_state->set('selected_year', $isoYear);
    $form_state->set('nid', $node->id());

    $form['oh_previous_week'] = [
      '#type' => 'submit',
      '#value' => $this->t('&lt;'),
      '#submit' => [
        [$this, 'previousWeek'],
      ],
    ];

    $form['oh_current_week'] = [
      '#type' => 'submit',
      '#value' => $this->t('Current week'),
      '#submit' => [
        [$this, 'currentWeek'],
      ],
    ];

    $form['oh_next_week'] = [
      '#type' => 'submit',
      '#value' => $this->t('&gt;'),
      '#submit' => [
        [$this, 'nextWeek']
      ],
    ];

    $form['oh_ui'] = [
      '#theme' => 'opening_hours_admin',
      '#interval' => $this->dateHelper->generateWeekInterval(
        $isoYear,
        $isoWeekNumber
      ),
      '#instances' => [2],
      '#week_number' => $isoWeekNumber,
      '#year' => $isoYear,
      '#prefix' => '<div id="opening-hours__node-admin-wrapper">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $a = 1;
  }

  public function currentWeek(array &$form, FormStateInterface $form_state) {
    $url = \Drupal\Core\Url::fromRoute('entity.node.opening_hours')
      ->setRouteParameters(['node' => $form_state->get('nid')]);

    $today = new \DateTime();

    $url->setOptions([
      'query' => [
        'year' => $today->format('o'),
        'week' => $today->format('W'),
      ]
    ]);

    $form_state->setRedirectUrl($url);
  }

  public function previousWeek(array &$form, FormStateInterface $form_state) {
    $url = \Drupal\Core\Url::fromRoute('entity.node.opening_hours')
      ->setRouteParameters(['node' => $form_state->get('nid')]);

    $url->setOptions([
      'query' => [
        'year' => $form_state->get('selected_year'),
        'week' => $form_state->get('selected_week') - 1,
      ]
    ]);

    $form_state->setRedirectUrl($url);
  }

  public function nextWeek(array &$form, FormStateInterface $form_state) {
    $url = \Drupal\Core\Url::fromRoute('entity.node.opening_hours')
      ->setRouteParameters(['node' => $form_state->get('nid')]);

    $url->setOptions([
      'query' => [
        'year' => $form_state->get('selected_year'),
        'week' => $form_state->get('selected_week') + 1,
      ]
    ]);

    $form_state->setRedirectUrl($url);
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Drupal\Core\Access\AccessResult
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(AccountInterface $account, NodeInterface $node) {
    $userHasAccess = $account->hasPermission('edit opening hours for content');
    /** @var \Drupal\node\NodeTypeInterface $node_type */
    $nodeType = NodeType::load($node->bundle());
    $nodeSupportsOpeningHours = (bool) $nodeType->getThirdPartySetting('opening_hours', 'oh_enabled', FALSE);

    return AccessResult::allowedIf($userHasAccess && $nodeSupportsOpeningHours);
  }
}

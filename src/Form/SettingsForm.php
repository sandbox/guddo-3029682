<?php

namespace Drupal\opening_hours\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * Administer opening hours settings.
 */
class SettingsForm extends ConfigFormBase {

  const FORM_ID = 'opening_hours.settings_form';

  const CONFIG_KEY = 'opening_hours.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      self::CONFIG_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_KEY);

    $form['blocked_title'] = array(
      '#tag' => 'h3',
      '#type' => 'html_tag',
      '#value' => $this->t('Blocked days'),
    );

    $form['blocked_description'] = array(
      '#tag' => 'p',
      '#type' => 'html_tag',
      '#value' => $this->t('Blocked days are days where no opening hours are
    allowed, ie. everything appears to be closed. You can use this
    functionality to enforce closing days for a large chain of shops or
    similar organisations, to avoid having to enter closing data on
    dozens of pages.'),
      '#attributes' => array('class' => 'description'),
    );

    $form['blocked_days_wrapper'] = [
      '#type' => 'container',
      '#prefix' => '<div id="opening-hours__blocked-days-wrapper">',
      '#suffix' => '</div>',
    ];

    if (empty($form_state->get('blocked_days_count'))) {
      $form_state->set('blocked_days_count', 1);
    }

    $form['blocked_days_wrapper']['blocked_days'] = [
      '#tree' => TRUE,
    ];
    $saved_blocked_days = $config->get('blocked_days') ?? [];
    $input_element_count = $form_state->get('blocked_days_count') + count($saved_blocked_days);
    for ($i = 0; $i < $input_element_count; $i++) {
      $form['blocked_days_wrapper']['blocked_days'][] = [
        '#type' => 'date',
        '#default_value' => $saved_blocked_days[$i] ?? '',
      ];
    }

    $form['add_more'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add more'),
      '#submit' => [
        [$this, 'addBlockedDayInputSubmit'],
      ],
      '#ajax' => [
        'callback' => [$this, 'addBlockedDayInputCallback'],
        'method' => 'replace',
        'wrapper' => 'opening-hours__blocked-days-wrapper',
      ]
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Custom submit handler.
   *
   * Increments element count and rebuilds the form.
   *
   * @see \Drupal\opening_hours\Form\SettingsForm::buildForm()
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function addBlockedDayInputSubmit(array $form, FormStateInterface $form_state) {
    $form_state->set('blocked_days_count', $form_state->get('blocked_days_count') + 1);
    $form_state->setRebuild();
  }

  /**
   * Form AJAX callback.
   *
   * Returns the element that needs update.
   *
   * @see \Drupal\opening_hours\Form\SettingsForm::buildForm()
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   Element to be replaced.
   */
  public function addBlockedDayInputCallback(array $form, FormStateInterface $form_state) {
    return $form['blocked_days_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $blocked_days = array_filter($values['blocked_days']);

    $this->config(self::CONFIG_KEY)
      ->set('blocked_days', $blocked_days)
      ->save();

    parent::submitForm($form, $form_state);
  }

}

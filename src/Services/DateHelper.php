<?php

namespace Drupal\opening_hours\Services;

class DateHelper {
  /**
   * @param int $year
   * @param int $week
   *
   * @return array
   * @throws \Exception
   */
  public function generateWeekInterval(int $year, int $week) {
    $_dt = new \DateTime();
    $_dt->setISODate($year, $week);

    // Reset time.
    $_dt->modify('today');

    $interval = [];
    for ($i = 1; $i <= 7; $i++) {
      $interval[] = $_dt->format('F d, l');
      $_dt->modify('next day');
    }

    return $interval;
  }
}
